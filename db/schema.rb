# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180124233752) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "points", force: :cascade do |t|
    t.integer "tracksegment_id"
    t.string "name"
    t.float "latitude"
    t.float "longitude"
    t.float "elevation"
    t.string "description"
    t.datetime "point_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tracksegment_id"], name: "index_points_on_tracksegment_id"
  end

  create_table "tracks", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "gpx_file_name"
    t.string "gpx_content_type"
    t.integer "gpx_file_size"
    t.datetime "gpx_updated_at"
  end

  create_table "tracksegments", force: :cascade do |t|
    t.integer "track_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["track_id"], name: "index_tracksegments_on_track_id"
  end

end
