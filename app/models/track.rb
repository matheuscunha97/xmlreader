class Track < ApplicationRecord
	attr_accessor :name, :gpx  
  	has_many :tracksegments, :dependent => :destroy
  	has_many :points, :through => :tracksegments
  	has_attached_file :gpx
end
