class CreateTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tracks do |t|
      t.string :name

      t.timestamps
    end
    add_attachment :tracks, :gpx
  end
end
